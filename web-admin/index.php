<?php 
include "../configs/config.php";
$message ="";
$template_admin = "../app/admin/";
	
	@$uname = $_POST['uname'];
	@$pass = md5($_POST['pass']); // md5 = untuk men-generate password.
	
		@$sql = "SELECT username, password, display_name, email FROM tb_users ";
	    @$exec_query = mysql_query($sql); // running sql.

	    @$row = mysql_fetch_array($exec_query); // eksekusi query menjadi data Array.
         // print_r($row);die();


	if ($uname === "") //Apabila username kosong.
	{ $message = "isikan username anda!";}

	elseif ($pass === "") //Apabila password kosong.
	{ $message = "isikan password anda!"; }

	elseif($uname !== $row['username']) // apabila username tidak cocok
	{ $message = "username anda tidak sesuai!";}

	elseif($pass !== $row['password']) // apabila password tidak cocok
	{ $message = "password anda tidak sesuai!"; }

	else{
		session_start();
		$_SESSION['welcome']  = "Selamat datang!";
		$_SESSION['name']     = $row['display_name'];
		$_SESSION['email']    = $row['email'];

		Echo '<script>window.location.href="dashboard.php";</script>';
	}

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> </title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo $template_admin ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo $template_admin ?>assets/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo $template_admin ?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

 
</head>
<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    <div class="panel-body">
                    	<?php 
                    		if ($message !== NULL) {
                    			echo $message;
                    		}
                    	?>

                        <form role="form" method="POST" action="index.php">
                            <fieldset> 
                                <div class="form-group">
                                    <input class="form-control" placeholder="Username" name="uname" type="text" autofocus>
                                </div>

                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="pass" type="password">
                                </div>
                                 
                                <!-- Change this to a button or input when using this as a form -->
                                <button type="submit" name="login" class="btn btn-lg btn-success btn-block">Sign in</button>
                                <a href="register.php">Register</a>

                            </fieldset>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo $template_admin ?>assets/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo $template_admin ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo $template_admin ?>assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo $template_admin ?>assets/dist/js/sb-admin-2.js"></script>
	 
    </body>
</html>