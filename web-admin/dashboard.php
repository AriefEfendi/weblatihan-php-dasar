<?php
	// use databases!
	include('../configs/config.php');

    // Aktifkan session.
    session_start();

    // menggunakan folder tempat penyimpanan halaman.
	$template_admin = "../app/admin/";
	$halaman 		= "";

	// panggil halaman yang telah kita daftarkan di routes.
	include '../configs/routes-admin.php';

	$template = $template_admin.$halaman;
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> </title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo $template_admin ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo $template_admin ?>assets/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <!-- <link href="<?php echo $template_admin ?>assets/css/timeline.css" rel="stylesheet"> -->

    <!-- Custom CSS -->
    <link href="<?php echo $template_admin ?>assets/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?php echo $template_admin ?>assets/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo $template_admin ?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

 
</head>
<body>




<?php include($template_admin.'navigasi.php') ?>
<?php include($template); ?>
	 



						

    <!-- jQuery -->
    <script src="<?php echo $template_admin ?>assets/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo $template_admin ?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo $template_admin ?>assets/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="<?php echo $template_admin ?>assets/vendor/raphael/raphael-min.js"></script>
    <script src="<?php echo $template_admin ?>assets/vendor/morrisjs/morris.min.js"></script>
    <script src="<?php echo $template_admin ?>assets/js/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo $template_admin ?>assets/js/sb-admin-2.js"></script>
	<script type="text/javascript">
    	$(document).ready(function() {
	        $('#dataTables-example').DataTable({
	            responsive: true
	        });

	        // event delete
	        $('.btn-delete').click(function(e) {
	            e.preventDefault();
	            var id = $(this).attr('data-id');
	            $("#id").val(id);
	            $('#delete-modal').modal('show');
	        });
	    });
    </script>
</body>
</html>