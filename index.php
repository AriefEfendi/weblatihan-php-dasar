<?php
	// use databases!
	include('configs/config.php');

    // Aktifkan session.
    session_start();

    // menggunakan folder tempat penyimpanan halaman.
	$template_public = "app/public/";
	$halaman 		= "";

	// panggil halaman yang telah kita daftarkan di routes.
	include 'configs/routes-public.php';

	$template = $template_public.$halaman;
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Freelancer</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo $template_public ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="<?php echo $template_public ?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Plugin CSS -->
    <link href="<?php echo $template_public ?>assets/vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="<?php echo $template_public ?>assets/css/freelancer.min.css" rel="stylesheet">
    <link href="<?php echo $template_public ?>assets/css/custom.css" rel="stylesheet">

  </head>

  <body id="page-top">




<?php include($template_public.'navigasi.php') ?>
<?php include($template); ?>
	 


<?php include($template_public.'footer.php'); ?>


	<!-- Bootstrap core JavaScript -->
    <script src="<?php echo $template_public; ?>assets/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo $template_public; ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo $template_public; ?>assets/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="<?php echo $template_public; ?>assets/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="<?php echo $template_public; ?>assets/js/jqBootstrapValidation.js"></script>
    <script src="<?php echo $template_public; ?>assets/js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="<?php echo $template_public; ?>assets/js/freelancer.min.js"></script>

</body>
</html>