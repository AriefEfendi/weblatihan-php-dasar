<?php 
    // cek ada session yang aktif apa tidak.
    if (@$_SESSION['name'] === null) {
        $url="../web-admin/";
          Echo '<script>window.location.href="' . $url . '";</script>';
      }

    // ambil data dari tabel yang berelasi, (modul prodak), untuk di tampilkan
    $query = "
            SELECT tb1.id, tb1.title,tb1.image, tb1.content, tb1.excerpt, tb1.price, tb1.created_at, tb1.updated_at, tb2.display_name as display_name2, tb3.display_name as display_name3 from tb_product as tb1, tb_category as tb2, tb_pages as tb3 where tb1.category_id = tb2.id and tb1.page_id = tb3.id
            ";

    $exec_query = mysql_query($query);
    // var_dump($exec_query);
?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">List Product</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    DataTables Advanced Tables
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body"> 
                     
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Title</th>
                                <th>Content</th>
                                <!-- <th>Avatar</th> -->
                                <th>Kutipan</th>
                                <th>Price</th>
                                <th>Categori</th>
                                <th>Page</th>
                                <!-- <th>Status</th> -->
                                <th>Image</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (count($exec_query > 0)) : 
                                $no = 1;
                                while ($data = mysql_fetch_array($exec_query)) :
                            ?>
                            <tr class="odd gradeX">
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $data['title'] ?></td>
                                <td><?php echo $data['content'] ?></td>
                                <td><?php echo $data['excerpt'] ?></td>
                                <td>Rp. <?php echo $data['price'] ?>,-</td>
                                <td><?php echo $data['display_name2'] ?></td>
                                <td><?php echo $data['display_name3'] ?></td> 
                                <!-- <td></td> -->
                                <td>
                                    <img src="../upload/gambar/<?php echo $data['image'] ?>" height="12%" /> 
                                </td>
                                <td class="center"><?php echo $data['created_at'] ?></td>
                                <td class="center"><?php echo $data['updated_at'] ?></td>
                                <td class="center">
                                    <a href="?page=edit-product&id=<?= $data['id'];?>" class="btn btn-primary">Edit</a> 
                                    <a href="?page=del-product&id=<?= $data['id'];?>" class="btn btn-danger btn-delete" data-id="<?php echo $data['id'] ?>">Delete</a>
                                </td>
                            </tr>
                            <?php
                                endwhile;
                            endif; ?>
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /#page-wrapper -->

<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Alert</h4>
      </div>
      <form action="?page=delete-product" method="post">
          <div class="modal-body">
            <p>Are you sure want to delete this?</p>
            <input type="hidden" name="id" id="id" value="">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            <button type="submit" class="btn btn-primary">Yes</button>
          </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


