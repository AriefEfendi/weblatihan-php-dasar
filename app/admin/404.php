<div class="jumbotron">
	<div class="container text-center">
		<h1>Halaman tidak ditemukan!</h1>
		<p><?= $title ?></p>
		<p>
			<a class="btn btn-primary btn-lg">Learn more</a>
		</p>
	</div>
</div>